<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScanlogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scanlogs', function (Blueprint $table) {
            $table->id();
            $table->string('pin', 250);
            $table->integer('verifymode');
            $table->integer('iomode');
            $table->integer('workcode');
            $table->dateTime('scan_date', 0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scanlogs');
    }
}
