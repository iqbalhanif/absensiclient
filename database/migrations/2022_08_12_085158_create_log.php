<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log', function (Blueprint $table) {
            $table->id();
            $table->string('type', 50)->default('log');
            $table->text('message');
            $table->text('request_data')->nullable();
            $table->timestamps();
        });

        Schema::table('scanlogs', function (Blueprint $table) {
            $table->integer('status')->after('scan_date')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log');
        Schema::table('scanlogs', function (Blueprint $table) {
            $table->dropColumn('status');
        });
    }
}
