<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\Scanlog;
use App\Models\Karyawan;
use App\Models\log;

class uploadAbsensi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absensi:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'mengupload absensi ke cloud database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        try {
            $api_token  = env("ABSENSI_API_TOKEN", 0);
            $client_id  = env("ABSENSI_CLIENT_ID", 0);
            $base_url   = env("ABSENSI_BASE_URL_SERVER", 0);
            $api_address= env("ABSENSI_API_ADDRESS", 0);
            
            $client = new Client([
                'base_uri' => $base_url,
                'timeout'  => 5.0,
            ]);

            $scanlogs =  Scanlog::where('status',0)->orderBy('id', 'asc')->get();
            Log::create([
                'type' => 'log',
                'message' => 'uploading '. count($scanlogs) .' scanlogs to shelter SIMA!',
            ]);

            echo "total ".count($scanlogs)."\n";
            
            foreach ($scanlogs as $key => $scanlog) {
                $response = $client->request('POST', $api_address, [
                    'headers' => [
                        'Accept'     => 'application/json',
                    ],
                    'http_errors' => false,
                    'form_params' => [
                        'api_token'   => $api_token,
                        'client_id'   => $client_id,
                        'id_user'     => $scanlog->pin,
                        'tanggal'     => $scanlog->scan_date,
                        'jenis_absen' => $scanlog->iomode,
                        'no_sn'       => $scanlog->no_sn
                    ]
                ]);

                if($response->getStatusCode() == 200){
                    $scanlog->status = 1;
                    $scanlog->save();
                    Log::create([
                        'type' => 'log',
                        'message' => 'upload success',
                        'request_data' => json_encode($scanlog)
                    ]);
                }
                else if($response->getStatusCode() == 401){
                    Log::create([
                        'type' => 'error',
                        'message' => 'upload skipped! Data ID Mesin not found!',
                        'request_data' => json_encode($scanlog)
                    ]);
                    echo "upload skipped! Data ID Mesin ".$scanlog->pin." not found!!\n";
                    continue;
                }
                else{
                    Log::create([
                        'type' => 'error',
                        'message' => 'upload failed, Error Code :'.$response->getStatusCode(),
                        'request_data' => json_encode($scanlog)
                    ]);
                    $response_obj = json_decode($response->getBody());
                    throw new \Exception('Response : '.$response_obj->message, 403);
                }
            }
            echo "upload absensi finished!";
        } catch (\Exception $e) {
            Log::create([
                'type' => 'error',
                'message' => 'upload failed, Error Code :'.$e->getMessage(),
                'request_data' => json_encode($e)
            ]);
            echo 'upload failed, Error message "'.$e->getMessage().'"';
        }
    }
}
