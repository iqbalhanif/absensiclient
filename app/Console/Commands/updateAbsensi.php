<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use GuzzleHttp\Client;
use App\Models\Scanlog;
use App\Models\Karyawan;
use App\Models\log;

class updateAbsensi extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'absensi:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update data Absensi';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $ip_address = env("ABSENSI_IP_ADDRESS_PC", 0);
        $sn_string  = env("ABSENSI_SN_MACHINE", 0);
        $sn_list    = explode(",", $sn_string);

        foreach ($sn_list as $key => $sn) {
            $client = new Client([
                'base_uri' => $ip_address,
                'timeout'  => 7.0,
            ]);

            Log::create([
                'type' => 'log',
                'message' => 'read new scanlog from fingerprint machine',
                'request_data' => $sn
            ]);

            $response = $client->request('POST','scanlog/new',[
                'form_params' => [
                    'sn' => $sn
                ]
            ]);

            $body = json_decode($response->getBody()->getContents());

            if(isset($body->Data)){
                $data = $body->Data;

                Log::create([
                    'type' => 'log',
                    'message' => 'new scanlog '. sizeof($data) . ' data found',
                    'request_data' => json_encode($data)
                ]);
                Log::create([
                    'type' => 'log',
                    'message' => 'writing scanlog data to local db'
                ]);

                foreach ($data as $value) {
                    $scanlog = new Scanlog;
                    $scanlog->pin = $value->PIN;
                    $scanlog->workcode = $value->WorkCode;
                    $scanlog->verifymode = $value->VerifyMode;
                    $scanlog->scan_date = $value->ScanDate;
                    $scanlog->iomode = $value->IOMode;
                    $scanlog->no_sn = $sn;
                    $scanlog->save();
                }
                echo "scanlog found and write to local db finished!";
            }
            else{
                echo "no new scanlog found";
            }


        }
    }



}
