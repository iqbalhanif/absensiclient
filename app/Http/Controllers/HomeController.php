<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use App\Models\Scanlog;
use App\Models\Karyawan;
use App\Models\log;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $ip_address = env("ABSENSI_IP_ADDRESS_PC", 0);
        $sn_string  = env("ABSENSI_SN_MACHINE", 0);
        $sn_list    = explode(",", $sn_string);

        foreach ($sn_list as $key => $sn) {
            $client = new Client([
                // Base URI is used with relative requests
                'base_uri' => $ip_address,
                // You can set any number of default request options.
                'timeout'  => 7.0,
            ]);

            Log::create([
                'type' => 'log',
                'message' => 'read new scanlog from fingerprint machine',
                'request_data' => $sn
            ]);

            $response = $client->request('POST','scanlog/all/paging',[
//            $response = $client->request('POST','scanlog/new',[
                'form_params' => [
                    'sn' => $sn
                ]
            ]);

            $body = json_decode($response->getBody()->getContents());

            if(isset($body->Data)){
                $data = $body->Data;

                Log::create([
                    'type' => 'log',
                    'message' => 'new scanlog '. sizeof($data) . ' data found',
                    'request_data' => json_encode($data)
                ]);
                Log::create([
                    'type' => 'log',
                    'message' => 'writing scanlog data to local db'
                ]);
                foreach ($data as $value) {
                    $scanlog = new Scanlog;
                    $scanlog->pin = $value->PIN;
                    $scanlog->workcode = $value->WorkCode;
                    $scanlog->verifymode = $value->VerifyMode;
                    $scanlog->scan_date = $value->ScanDate;
                    $scanlog->iomode = $value->IOMode;
                    $scanlog->save();
                }
                dd("scanlog found and write to local db finished!");
            }
            else{
                dd("no scanlog found");
            }
        }
    }
}
