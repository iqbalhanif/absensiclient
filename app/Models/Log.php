<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $table = 'log';

    protected $fillable = [
        'type',
        'message',
        'request_data',
        'created_at',
        'updated_at'
    ];
    
}
