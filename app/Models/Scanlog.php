<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Scanlog extends Model
{
    use SoftDeletes;
    protected $table = 'scanlogs';

    protected $fillable = [
        'no_sn',
        'pin',
        'verifymode',
        'iomode',
        'workcode',
        'scan_date',
        'status',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
}
