<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Karyawan extends Model
{   
    protected $table = 'karyawans';

    protected $fillable = [
        'pin',
        'password',
        'rfid',
        'privilege',
        'name',
        'created_at',
        'updated_at',
    ];
}
